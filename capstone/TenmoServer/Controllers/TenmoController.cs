﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TenmoServer.DAO;
using TenmoServer.Models;

namespace TenmoServer.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class TenmoController : ControllerBase
    {
        public IAccountDao accountDao;
        public IUserDao userDao;
        public ITransferDao transferDao;


        public TenmoController(IAccountDao accountDao, IUserDao userDao, ITransferDao transferDao)
        {
            this.accountDao = accountDao;
            this.userDao = userDao;
            this.transferDao = transferDao;

        }

        [HttpGet("{id}")]
        public ActionResult<decimal> GetAccountBalance(int id)
        {
            ActionResult<decimal> result = 0;
            if (userDao.GetUserById(id) == null)
            {
                result = NotFound("User not found.");
            }
            else
            {
                result = Ok(accountDao.GetAccountBalance(id));
            }

            return result;
        }
        [HttpGet("users")]
        public ActionResult<IList<User>> GetUsersIdName()
        {
            IList<User> result = null;

            result = userDao.GetUsersIdName(); //Queries user list without password hash & salt


            return Ok(result);
        }

        [HttpGet("transfers")]
        public ActionResult<List<Transfer>> GetTransfersByUser()
        {

            User user = userDao.GetUserByUsername(HttpContext.User.Identity.Name);
            int userId = user.UserId;
            int accountId = accountDao.GetAccountIdByUser(userId);

            List<Transfer> result = null;
            result = transferDao.GetTransfers(accountId);


            return Ok(result);
        }

        [HttpPost("transfers")]
        public ActionResult<Transfer> PostTransferSend(TransferClient transferFromClient)
        {
            //build complete transfer object from client data
            Transfer transferToSend = MapTransferClientToTransfer(transferFromClient);

            //validate object
            //if invalid - return badrequest
            if (!ValidateTransfer(transferToSend))
            {
                return BadRequest(transferToSend);
                
            }
            else
            {
                Transfer result = transferDao.PostTransferSend(transferToSend);

                if (result.TransferId != 0)
                {
                    accountDao.UpdateBalances(result);
                }
                return Ok(result);
            }



        }

        [HttpGet("myaccount")]
        public ActionResult<int> GetAccountIdByUsername()
        {
            ActionResult<int> result = 0;
            if (userDao.GetUserByUsername(HttpContext.User.Identity.Name) == null)
            {
                result = NotFound("User not found.");
            }
            result = userDao.GetAccountIdByUsername(HttpContext.User.Identity.Name);

            return result;
        }
        [HttpGet("users/accounts/{accountId}")]
        public ActionResult<string> GetTransferUsernameByAccountId(int accountId)
        {
            ActionResult<string> result = "";
            
            result = userDao.GetUsernameByAccountId(accountId);

            return result;
        }

        

        private Transfer MapTransferClientToTransfer(TransferClient transferFromClient)
        {
            Transfer result = new Transfer();
            //map client-given fields
            result.Amount = transferFromClient.Amount;
            result.TransferTypeId = transferFromClient.TransferTypeId;
            result.AccountTo = accountDao.GetAccountIdByUser(transferFromClient.User_Target);
            //map other fields
            int userId = userDao.GetUserByUsername(HttpContext.User.Identity.Name).UserId; //retrieve username from HTTPContext and lookup userid
            result.AccountFrom = accountDao.GetAccountIdByUser(userId); //for request transfers, add if-then to flip account_from account_to
            if (transferFromClient.TransferTypeId == 2)
            {
                result.TransferStatusId = 2; //send transfers are auto approved

            }
            else
            {
                result.TransferStatusId = 1; //else set to pending
            }

            return result;
        }


        private bool ValidateTransfer(Transfer transfer)
        {
            bool result = true;

            //get requesting users ID
            int userId = userDao.GetUserByUsername(HttpContext.User.Identity.Name).UserId;

            if (transfer.Amount <= 0) //amount must be positive
            {
                result = false;
            }
            if (transfer.Amount > accountDao.GetAccountBalanceByAcctId(transfer.AccountFrom)) //amount must be less than account_from
            { //todo do we need to update this for receive money transfers???
                result = false;
            }
            if (transfer.AccountFrom == transfer.AccountTo) //account cannot be the same
            {
                result = false;
            }
            //check if user sending the request has their own account in _to field, for send money xfers
            if (transfer.TransferTypeId == 2 && transfer.AccountTo == accountDao.GetAccountIdByUser(userId))
            {
                result = false;
            }


            return result;
        }
    }
}
