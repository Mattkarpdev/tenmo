﻿using System.Collections.Generic;
using TenmoServer.Models;

namespace TenmoServer.DAO
{
    public interface ITransferDao
    {

        public Transfer GetTransferById(int transferId);

        public List<Transfer> GetTransfers(int accountId);

        public Transfer PostTransferSend(Transfer transferToSend);
        

            
    }
}
