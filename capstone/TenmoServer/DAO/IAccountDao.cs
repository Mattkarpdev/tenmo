﻿using TenmoServer.Models;

namespace TenmoServer.DAO
{
    public interface IAccountDao
    {
        public decimal GetAccountBalance(int userId);
        public int GetAccountIdByUser(int userId);
        public void UpdateBalances(Transfer transfer);
        public decimal GetAccountBalanceByAcctId(int accountId);
    }
}
