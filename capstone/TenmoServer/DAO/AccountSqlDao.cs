﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using TenmoServer.Exceptions;
using TenmoServer.Models;

namespace TenmoServer.DAO
{
    public class AccountSqlDao : IAccountDao
    {

        private readonly string connectionString;
        //constructor
        public AccountSqlDao(string dbConnectionString)
        {
            connectionString = dbConnectionString;
        }

        public decimal GetAccountBalance(int userId)
        {
            decimal balance = 0;

            string sql = @"SELECT balance
                            FROM account 
                            WHERE user_id = @user_id";
            
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@user_id", userId);
                    balance = Convert.ToDecimal( cmd.ExecuteScalar());
                }
            }
            catch (SqlException ex)
            {
                throw new DaoException("SQL exception occurred", ex);
            }

            return balance;
        }
        public decimal GetAccountBalanceByAcctId(int accountId)
        {
            decimal balance = 0;

            string sql = @"SELECT balance
                            FROM account 
                            WHERE account_id = @account_id";

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@account_id", accountId);
                    balance = Convert.ToDecimal(cmd.ExecuteScalar());
                }
            }
            catch (SqlException ex)
            {
                throw new DaoException("SQL exception occurred", ex);
            }

            return balance;
        }

        public int GetAccountIdByUser(int userId)
        {
            int result = 0;
            string sql = @"SELECT account_id
                            FROM account 
                            WHERE user_id = @user_id";

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@user_id", userId);
                    result = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            catch (SqlException ex)
            {
                throw new DaoException("SQL exception occurred", ex);
            }

            return result;

        }

        public void UpdateBalances(Transfer transfer)
        {
            
            string sqlTo = "UPDATE account SET balance += @amount WHERE account_id = @account_to"; 
            string sqlFrom = "UPDATE account SET balance -= @amount WHERE account_id = @account_from";

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmdTo = new SqlCommand(sqlTo, conn);
                    cmdTo.Parameters.AddWithValue("@amount", transfer.Amount);
                    cmdTo.Parameters.AddWithValue("@account_to", transfer.AccountTo);
                    cmdTo.ExecuteNonQuery();

                    SqlCommand cmdFrom = new SqlCommand(sqlFrom, conn);
                    cmdFrom.Parameters.AddWithValue("@amount", transfer.Amount);
                    cmdFrom.Parameters.AddWithValue("@account_from", transfer.AccountFrom);
                    cmdFrom.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                throw new DaoException("SQL exception occurred", ex);
            }

          

        }
    }
}
