﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using TenmoServer.Exceptions;
using TenmoServer.Models;

namespace TenmoServer.DAO
{
    public class TransferSqlDao : ITransferDao
    {
        private readonly string connectionString;
        

        public TransferSqlDao(string dbConnectionString)
        {
            connectionString = dbConnectionString;
        }
        public Transfer GetTransferById(int transferId)
        {
            Transfer result = null;

            string sql = "SELECT transfer_id, transfer_type_id, transfer_status_id, account_from, account_to, amount  " +
                "FROM transfer WHERE transfer_id = @transfer_id";


            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@transfer_id", transferId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        result = MapRowToTransfer(reader);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new DaoException("SQL exception occurred", ex);
            }

            return result;

        }

        public List<Transfer> GetTransfers(int accountId)
        {
            List<Transfer> result = new List<Transfer>();
            

            string sql = "SELECT transfer_id, transfer_type_id, transfer_status_id, account_from, account_to, amount  " +
                "FROM transfer WHERE account_from = @account_from OR account_to = @account_to;";


            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@account_from", accountId);
                    cmd.Parameters.AddWithValue("@account_to", accountId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(MapRowToTransfer(reader));
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new DaoException("SQL exception occurred", ex);
            }

            return result;
        }
        public Transfer PostTransferSend(Transfer transferToSend)
        {
            Transfer result = new Transfer();

            int accountFrom = transferToSend.AccountFrom;
            int accountTo = transferToSend.AccountTo;
            decimal amount = transferToSend.Amount;


            string sql = @"INSERT INTO transfer(transfer_type_id, transfer_status_id, account_from, account_to, amount)
                            OUTPUT INSERTED.transfer_id
                            VALUES(2, 2, @account_from, @account_to, @amount);";


            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@account_from", accountFrom);
                    cmd.Parameters.AddWithValue("@account_to", accountTo);
                    cmd.Parameters.AddWithValue("@amount", amount);
                    int transferId = Convert.ToInt32(cmd.ExecuteScalar());
                    result = GetTransferById(transferId);
                   
                }
            }
            catch (SqlException ex)
            {
                throw new DaoException("SQL exception occurred", ex);
            }




            return result;
        }

        private Transfer MapRowToTransfer(SqlDataReader reader)
        {
            Transfer transfer = new Transfer();
           transfer.TransferId = Convert.ToInt32(reader["transfer_id"]);
            transfer.TransferTypeId = Convert.ToInt32(reader["transfer_type_id"]);
            transfer.TransferStatusId = Convert.ToInt32(reader["transfer_status_id"]);
            transfer.AccountFrom = Convert.ToInt32(reader["account_from"]);
            transfer.AccountTo = Convert.ToInt32(reader["account_to"]);
            transfer.Amount = Convert.ToDecimal(reader["amount"]);

            return transfer;
            
        }
    }
}
