﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TenmoClient.Models
{
    public class TransferClient
    {
            //The incomplete transfer object sent to the server

            public int TransferTypeId { get; set; }

            public int User_Target { get; set; }

            public decimal Amount { get; set; }

            public TransferClient()
            {

            }

    }
}
