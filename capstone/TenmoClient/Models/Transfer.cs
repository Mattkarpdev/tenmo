﻿namespace TenmoClient.Models
{
    public class Transfer
    {
        public int TransferId { get; set; }

        public int TransferTypeId { get; set; }

        public int TransferStatusId { get; set; }

        public int AccountFrom { get; set; }

        public int AccountTo { get; set; }

        public decimal Amount { get; set; }

  

        public Transfer()
        {

        }

        public Transfer(int transferType, int accountFrom, int accountTo, decimal amount)
        {
            TransferTypeId = transferType;
            AccountFrom = accountFrom;
            AccountTo = accountTo;
            Amount = amount;

        }

        
        

        
    }


}
