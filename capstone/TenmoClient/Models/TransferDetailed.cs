﻿namespace TenmoClient.Models
{
    public class TransferDetailed
    {
        public int TransferId { get; set; }

        public int TransferTypeId { get; set; }

        public int TransferStatusId { get; set; }

        public int AccountFrom { get; set; }

        public int AccountTo { get; set; }

        public decimal Amount { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public int Transfer_Description { get; set; }

        public int Transfer_Status { get; set; }

        public int AccountId { get; set; }

        public TransferDetailed()
        {

        }

        public TransferDetailed(int transferType, int accountFrom, int accountTo, decimal amount, 
            int userId, string userName, int accountId)
        {
            TransferTypeId = transferType;
            AccountFrom = accountFrom;
            AccountTo = accountTo;
            Amount = amount;
            UserId = userId;
            UserName = userName;
            AccountId = accountId;
        }


    }


}
