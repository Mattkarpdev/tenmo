﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Channels;
using TenmoClient.Models;
using TenmoClient.Services;


namespace TenmoClient
{
    public class TenmoApp
    {
        private readonly TenmoConsoleService console = new TenmoConsoleService();
        private readonly TenmoApiService tenmoApiService;


        public TenmoApp(string apiUrl)
        {
            tenmoApiService = new TenmoApiService(apiUrl);
        }

        public void Run()
        {
            bool keepGoing = true;
            while (keepGoing)
            {
                // The menu changes depending on whether the user is logged in or not
                if (tenmoApiService.IsLoggedIn)
                {
                    keepGoing = RunAuthenticated();
                }
                else // User is not yet logged in
                {
                    keepGoing = RunUnauthenticated();
                }
            }
        }

        private bool RunUnauthenticated()
        {
            console.PrintLoginMenu();
            int menuSelection = console.PromptForInteger("Please choose an option", 0, 2, 1);
            while (true)
            {
                if (menuSelection == 0)
                {
                    return false;   // Exit the main menu loop
                }

                if (menuSelection == 1)
                {
                    // Log in
                    Login();
                    return true;    // Keep the main menu loop going
                }

                if (menuSelection == 2)
                {
                    // Register a new user
                    Register();
                    return true;    // Keep the main menu loop going
                }
                console.PrintError("Invalid selection. Please choose an option.");
                console.Pause();
            }
        }

        private bool RunAuthenticated()
        {
            console.PrintMainMenu(tenmoApiService.Username);
            int menuSelection = console.PromptForInteger("Please choose an option", 0, 6);
            if (menuSelection == 0)
            {
                // Exit the loop
                return false;
            }

            if (menuSelection == 1)
            {
                // View your current balance

                decimal balance = tenmoApiService.GetAccountBalance(tenmoApiService.UserId);
                //todo move to method in tenmo console service
                Console.WriteLine($"Your current account balance is: {balance:C2}");
                console.Pause();
            }

            if (menuSelection == 2)
            {
                // View your past transfers
                List<Transfer> transferList = tenmoApiService.GetTransfersByUser();
                int myAccount = tenmoApiService.GetMyAccountId();

                Console.WriteLine("-------------------------------------------");
                Console.WriteLine("Transfers");
                Console.WriteLine($"ID".PadRight(10) + "From/To".PadRight(23) + "Amount".PadRight(8));
                Console.WriteLine("-------------------------------------------");
                foreach (Transfer item in transferList)
                {
                    if (item.AccountFrom == myAccount)
                    {
                        string amount = item.Amount.ToString("C2");
                        Console.WriteLine($"{item.TransferId.ToString().PadRight(10)}" +
                            $"To:   {tenmoApiService.GetTransferUsernameByAccountId(item.AccountTo).ToString().PadRight(17)}" +
                            $"{amount.PadRight(8)}");

                    }
                    else if (item.AccountTo == myAccount)
                    {
                        string amount = item.Amount.ToString("C2");
                        Console.WriteLine($"{item.TransferId.ToString().PadRight(10)}" +
                          $"From: {tenmoApiService.GetTransferUsernameByAccountId(item.AccountFrom).ToString().PadRight(17)}" +
                          $"{amount.PadRight(8)}");
                    }
                }
                console.Pause();
                //todo prompt for transx 
                int idSelection = console.PromptForInteger("Please enter transfer ID to view details (0 to cancel): ");
                while (idSelection != 0)
                {
                    foreach (Transfer item in transferList)
                    {
                        if (item.TransferId == idSelection)
                        {

                            Console.WriteLine("--------------------------------------------");
                            Console.WriteLine("Transfer Details");
                            Console.WriteLine("--------------------------------------------");
                            Console.WriteLine($"Id: {item.TransferId}");
                            Console.WriteLine($"From: {tenmoApiService.GetTransferUsernameByAccountId(item.AccountFrom)} ");
                            Console.WriteLine($"To: {tenmoApiService.GetTransferUsernameByAccountId(item.AccountTo)} ");
                            string typedescr = item.TransferTypeId == 1 ? "Request" : (item.TransferTypeId == 2 ? "Send": "Unknown"); //hard code descriptions for now
                            Console.WriteLine($"Type: {typedescr}");
                            string statusdescr = item.TransferStatusId == 1 ? "Pending":( item.TransferStatusId == 2 ? "Approved": "Rejected");//hard code descriptions for now
                            Console.WriteLine($"Status: {statusdescr} ");
                            Console.WriteLine($"Amount: {item.Amount:C2} ");
                            console.Pause();
                        }

                    }
                    console.Pause();
                    idSelection = console.PromptForInteger("Please enter transfer ID to view details (0 to cancel): ");


                }
                //todo show transx detail

            }

            if (menuSelection == 3)
            {
                // View your pending requests
            }

            if (menuSelection == 4)
            {
                // Send TE bucks
                // display list of users except yourself
                Console.WriteLine("| --------------Users--------------- |");
                Console.WriteLine("|    Id  | Username                  |");
                Console.WriteLine("| -------+---------------------------|");
                List<ApiUser> userList = tenmoApiService.GetUsersIdName();

                foreach (ApiUser user in userList)
                {
                    if (user.UserId != tenmoApiService.UserId) //don't print active user
                    {
                        Console.WriteLine($"|  {user.UserId}  | {user.Username.PadRight(26)}|");

                    }
                }
                Console.WriteLine("| -------+---------------------------|");
                int targetUser = console.PromptForInteger("ID of the user you are sending to");

                decimal amount = console.PromptForDecimal("Enter amount to send");

                TransferClient tClient = new TransferClient();

                tClient.TransferTypeId = 2;
                tClient.User_Target = targetUser;
                tClient.Amount = amount;

                Transfer tResponse = null;
                try
                {
                    tResponse = tenmoApiService.PostTransferSend(tClient);

                }
                catch (Exception ex)
                {
                    console.PrintError(ex.Message);

                }
                if (tResponse != null)
                {
                    console.PrintSuccess($"Transfer successful:{tResponse.Amount:c}");

                }

                console.Pause();




                //todo add any client-side validations

            }

            if (menuSelection == 5)
            {
                // Request TE bucks
            }

            if (menuSelection == 6)
            {
                // Log out
                tenmoApiService.Logout();
                console.PrintSuccess("You are now logged out");
            }

            return true;    // Keep the main menu loop going
        }

        private void Login()
        {
            LoginUser loginUser = console.PromptForLogin();
            if (loginUser == null)
            {
                return;
            }

            try
            {
                ApiUser user = tenmoApiService.Login(loginUser);
                if (user == null)
                {
                    console.PrintError("Login failed.");
                }
                else
                {
                    console.PrintSuccess("You are now logged in");
                    Console.WriteLine($"your token is {user.Token}"); //print token for development testing purposes todo: delete this
                }
            }
            catch (Exception)
            {
                console.PrintError("Login failed.");
            }

            console.Pause();
        }

        private void Register()
        {
            LoginUser registerUser = console.PromptForLogin();
            if (registerUser == null)
            {
                return;
            }
            try
            {
                bool isRegistered = tenmoApiService.Register(registerUser);
                if (isRegistered)
                {
                    console.PrintSuccess("Registration was successful. Please log in.");
                }
                else
                {
                    console.PrintError("Registration was unsuccessful.");
                }
            }
            catch (Exception)
            {
                console.PrintError("Registration was unsuccessful.");
            }
            console.Pause();
        }
    }
}
