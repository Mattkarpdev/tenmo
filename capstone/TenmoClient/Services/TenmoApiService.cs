﻿using RestSharp;
using System.Collections.Generic;
using TenmoClient.Models;


namespace TenmoClient.Services
{
    public class TenmoApiService : AuthenticatedApiService
    {
        public readonly string ApiUrl;

        public TenmoApiService(string apiUrl) : base(apiUrl) { }

        public decimal GetAccountBalance(int userId)
        {
            decimal balance = 0;
            RestRequest request = new RestRequest($"tenmo/{userId}");
            IRestResponse<decimal> response = client.Get<decimal>(request);

            CheckForError(response);
            return response.Data;
        }

        public List<ApiUser> GetUsersIdName()
        {
            List<ApiUser> result = new List<ApiUser>();
            RestRequest request = new RestRequest("tenmo/users");
            IRestResponse<List<ApiUser>> response = client.Get<List<ApiUser>>(request);

            CheckForError(response);

            result = response.Data;
            return result;
        }

        public Transfer PostTransferSend(TransferClient tClient)
        {
            RestRequest request = new RestRequest("tenmo/transfers");
            request.AddJsonBody(tClient);

            IRestResponse<Transfer> response = client.Post<Transfer>(request);
            CheckForError(response);
            return response.Data;

        }

        public List<Transfer> GetTransfersByUser()
        {
            //List<Transfer> result = new List<Transfer>();
            RestRequest request = new RestRequest("tenmo/transfers");
            IRestResponse<List<Transfer>> response = client.Get<List<Transfer>>(request);

            CheckForError(response);

            List<Transfer> result = response.Data;
            return result;
        }

        public int GetMyAccountId()
        {
            int id = 0;
            RestRequest request = new RestRequest($"tenmo/myaccount");
            IRestResponse<int> response = client.Get<int>(request);

            CheckForError(response);
            id = response.Data;
            return id;
        }

        public string GetTransferUsernameByAccountId(int accountId)
        {
            string username = null;
            RestRequest request = new RestRequest($"tenmo/users/accounts/{accountId}");
            IRestResponse<string> response = client.Get<string>(request);

            CheckForError(response);
           username = response.Data;
            return username;
        }
    }
}
